console.log("Hello World");

let openers = document.querySelectorAll('.upper')
let upperInside = document.querySelectorAll('.upper > *')

openers.forEach(opener => {
    opener.addEventListener('click',(e) => {
        let question = e.target.parentElement

        console.log(question)
        if(question.classList.contains('active')) {
            question.classList.remove('active')
        } else {
            openers.forEach(op => {
                op.parentElement.classList.remove('active')
            })
            question.classList.add('active')
        }
    })
})

/*upperInside.forEach(ui => {
    ui.addEventListener('click',() => {
        ui.parentElement.click()
    })
})*/